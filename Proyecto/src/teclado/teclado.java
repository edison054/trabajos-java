package teclado;
import java.io.*;
public class teclado {
    private BufferedReader dato;
    private String datocadena;
    //CONSTRUCTOR DE LA CLASE TECLADO
    public teclado(){
        dato=new BufferedReader(new InputStreamReader(System.in));
        try{
            datocadena= dato.readLine();
        }
        catch(Exception e){
            System.out.println("Ocurrio un error...:(");
        }
    }
    //metodo que retorna lo leido desde el teclado en cadena 
    public String cadena(){
        return datocadena;
    }
    //metodo que retorna lo leido desde el teclado en nuemro entero
    public int entero(){
        return Integer.parseInt(datocadena);
    }
    //metodo que reotrna lo leido desde el teclado en double
   public double real(){
       return Double.parseDouble(datocadena);
   }
   //metodo que retorna lo leido desde el teclado en un caracter
  public char caracter(){
      return datocadena.charAt(0);
  }
  //metodo que retorna lo leido desde el teclado en float
  public float flotante(){
      return Float .parseFloat(datocadena);
  }
  //metodo que retorna lo leido desde el teclado en entero corto
  public short corto(){
      return Short.parseShort(datocadena);
  }
    
}
